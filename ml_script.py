from matplotlib import mlab, pyplot
import numpy
import psycopg2
import pylab
from textstat.textstat import textstat

try:
    conn = psycopg2.connect("dbname='jira_dataset' user='jira_user' host='localhost' password=''")
except:
    print("Unable to connect to the database")

def get_issues():
    issues = {}

    cur = conn.cursor()
    cur.execute("""SELECT id, status, created, resolved, description FROM jira_issue_report""")

    rows = cur.fetchall()
    for row in rows:
        # we do not want to train on unresolved tickets
        if not row[3]:
            continue
        id = row[0]
        description = row[4]
        age = (row[3] - row[2]).days
        wordcount = textstat.lexicon_count(description)
        if description and wordcount != 0:
            readability = textstat.flesch_reading_ease(description)
        else:
            # empty ticket readability kludge, discussed in dissertation
            readability = 206.835
        cur.execute("""SELECT count(id) FROM jira_issue_comment WHERE issue_report_id = """ + str(id))
        comments = cur.fetchone()[0]
        cur.execute("""SELECT count(id) FROM jira_issue_changelog_item WHERE issue_report_id = """ + str(id))
        volatility = float(cur.fetchone()[0])
        if age != 0:
            volatility = volatility / float(age)
        issues[id] = {
                         "age": age,
                         "wordcount": wordcount,
                         "readability": readability,
                         "comments": comments,
                         "volatility": volatility
                     }

    return issues

def build_matrices(issues):
    t = []
    X = []
    for id in issues.keys():
        t = t + [[issues[id]["age"]]]
        wordcount = issues[id]["wordcount"]
        readability = issues[id]["readability"]
        comments = issues[id]["comments"]
        volatility = issues[id]["volatility"]
        X.append([1,
                  (0.01 + float(wordcount))**-3, (0.01 + float(wordcount))**-2, (0.01 + float(wordcount))**-1, wordcount, wordcount**2,
                  numpy.exp(readability), readability, readability**2, readability**3, readability**4,
                  (0.01 + float(comments))**-1, comments, comments**2, comments**3, comments**4,
                  (0.01 + float(volatility))**-1, volatility, volatility**2, volatility**3, volatility**4
                 ])
    return (numpy.array(t), numpy.array(X))

print("Acquiring issues...")
issues = get_issues()

print("Building t and X matrices...")
t, X = build_matrices(issues)

print("Calculating w vector...")
XX = numpy.dot(X.T, X)
Xt = numpy.dot(X.T, t)
w = numpy.linalg.solve(XX, Xt)
print(w)

print("Creating plot of retrospectively predicted age v. actual age of tickets...")
predicted_t = numpy.dot(X, w)
pyplot.plot(t, predicted_t, 'rx')
pyplot.xlabel('actual age of ticket at resolving')
pyplot.ylabel('predicted age of ticket at resolving')
pyplot.show()

print("Calculating R-squared...")
SSreg = ((predicted_t - t.mean()) ** 2).sum()
SStot = ((t - t.mean()) ** 2).sum()
R2 = SSreg/SStot
print("R2 = %r" % R2)

print("Performing k-fold cross-validation...")
k = 10
N = len(X)
sizes = numpy.arange(0, N, N//k)
R2s = []
for i in range(k):
    X_test = X[sizes[i]:sizes[i+1]]
    X_train = numpy.delete(X, numpy.arange(sizes[i], sizes[i+1], 1), 0)
    t_test = t[sizes[i]:sizes[i+1]]
    t_train = numpy.delete(t, numpy.arange(sizes[i], sizes[i+1], 1), 0)
    XX_train = numpy.dot(X_train.T, X_train)
    Xt_train = numpy.dot(X_train.T, t_train)
    w_test = numpy.linalg.solve(XX_train, Xt_train)
    t_test_pred = numpy.dot(X_test, w_test)
    SSreg_test = ((t_test_pred - t_test.mean()) ** 2).sum()
    SStot_test = ((t_test - t_test.mean()) ** 2).sum()
    R2s.append(SSreg_test/SStot_test)
print("Cross-validation mean R2: %r,  standard deviation: %r" % (numpy.mean(R2s), numpy.std(R2s)))
