from matplotlib import mlab, pyplot
import psycopg2
import pylab
from textstat.textstat import textstat

try:
    conn = psycopg2.connect("dbname='jira_dataset' user='jira_user' host='localhost' password=''")
except:
    print("Unable to connect to the database")

def get_issues():
    issues = {}

    cur = conn.cursor()
    cur.execute("""SELECT id, status, created, resolved, description FROM jira_issue_report""")

    rows = cur.fetchall()
    for row in rows:
        # we do not want to train on unresolved tickets
        if not row[3]:
            continue
        id = row[0]
        description = row[4]
        age = (row[3] - row[2]).days
        wordcount = textstat.lexicon_count(description)
        if description and wordcount != 0:
            readability = textstat.flesch_reading_ease(description)
        else:
            # empty ticket readability kludge, discussed in dissertation
            readability = 206.835
        cur.execute("""SELECT count(id) FROM jira_issue_comment WHERE issue_report_id = """ + str(id))
        comments = cur.fetchone()[0]
        cur.execute("""SELECT count(id) FROM jira_issue_changelog_item WHERE issue_report_id = """ + str(id))
        volatility = float(cur.fetchone()[0])
        if age != 0:
            volatility = volatility / float(age)
        issues[id] = {
                         "age": age,
                         "wordcount": wordcount,
                         "readability": readability,
                         "comments": comments,
                         "volatility": volatility
                     }

    return issues

def get_plot_dimensions(issues):
    wordcounts = []
    readabilities = []
    comments = []
    volatilities = []
    ages = []
    for id in issues.keys():
        ages.append(issues[id]["age"])
        wordcounts.append(issues[id]["wordcount"])
        readabilities.append(issues[id]["readability"])
        comments.append(issues[id]["comments"])
        volatilities.append(issues[id]["volatility"])
    return (wordcounts, readabilities, comments, volatilities, ages)

print("Acquiring issues...")
issues = get_issues()

print("Calculating plot dimensions...")
wordcounts, readabilities, comments, volatilities, ages = get_plot_dimensions(issues)

print("Creating plots...")
f, ((plot1, plot2), (plot3, plot4)) = pyplot.subplots(2,2)

plot1.plot(wordcounts, ages, 'rx')
plot1.set_xlabel('word count of ticket')
plot1.set_ylabel('age of ticket at resolving')

plot2.plot(readabilities, ages, 'rx')
plot2.set_xlabel('Flesch readability of ticket')
plot2.set_ylabel('age of ticket at resolving')

plot3.plot(comments, ages, 'rx')
plot3.set_xlabel('number of comments of ticket')
plot3.set_ylabel('age of ticket at resolving')

plot4.plot(volatilities, ages, 'rx')
plot4.set_xlabel('volatility (issue change events per day) of ticket')
plot4.set_ylabel('age of ticket at resolving')

f.subplots_adjust(hspace=0.3)
pyplot.show()
