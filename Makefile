all: default
default: consentform dissertation
consentform:
	pdflatex -halt-on-error consentform
dissertation:
	pdflatex -halt-on-error -shell-escape dissertation
	bibtex dissertation
	pdflatex -halt-on-error -shell-escape dissertation
	pdflatex -halt-on-error -shell-escape dissertation
clean:
	rm -f consentform.pdf
	rm -f dissertation.pdf
	rm -f *.aux *.bbl *.blg *.dvi
